<HTML>
<TITLE> Сам. работа 2 - Шаихов Х.И. ПИ-323</TITLE>
<BODY>
<TABLE>
<?php
$N = rand(1, 499);
echo 'N = ' . $N . '<br>';
for ($i = 1; pow($i, 2) <= $N; $i++) {
    for ($j = 1; pow($j, 2) <= $N; $j++) {
        if (pow($i, 2) + pow($j, 2) == $N) {
            echo 'Можно представить в виде суммы квадратов ' . $i . '^2 и ' . $j . '^2';
            return true;
        }
    }
}

echo 'Число ' . $N . ' невозможно представить в виде суммы квадратов двух целых чисел';
return false;
?>
</TABLE>
</BODY>
</HTML>